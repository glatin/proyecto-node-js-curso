var mymap = L.map('mapid').setView([-34.4505406,-58.6016189,13], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors' 
}).addTo(mymap);

L.marker([-34.4502356,-58.5422076,17]).addTo(mymap);
L.marker([-34.4514569,-58.5432563,17]).addTo(mymap);

